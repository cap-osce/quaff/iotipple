#include <Arduino.h>
#include <WiFi.h>
#include <Adafruit_NeoPixel.h>
#include <ArduinoJson.h>

#define PIXEL_COUNT 60
#define PIXEL_SIZE 4
#define GAP_SIZE 2

#define QUERY_HOST "quaffacade.quaff.harmelodic.com"
#define QUERY_PORT 80
#define QUERY_PATH "/quaff/kubernetes/quaff-dev"

Adafruit_NeoPixel strip = Adafruit_NeoPixel(PIXEL_COUNT, 12, NEO_GRB + NEO_KHZ400);

void setAllPixels(uint32_t color) {
    for(int index = 0; index < PIXEL_COUNT; index++) {
        strip.setPixelColor(index, color);
        strip.show();
        delay(500/PIXEL_COUNT);
    }
}

void setup() {
    strip.begin();
    delay(500);
    strip.clear();
    delay(500);
    
    setAllPixels(strip.Color(255,0,0));
    setAllPixels(strip.Color(255,255,0));
    setAllPixels(strip.Color(0,255,0));
    setAllPixels(strip.Color(0,255,255));
    setAllPixels(strip.Color(0,0,255));
    setAllPixels(strip.Color(255,255,255));
    setAllPixels(strip.Color(0,0,0));

    Serial.begin(115200);
    WiFi.begin("DrayTek", NULL);

    boolean connected = false;

    while(!connected) {
        if (WiFi.status() == WL_CONNECT_FAILED) {
            Serial.println("Failed to connect to wifi");
        } else if (WiFi.status() == WL_CONNECTED) {
            Serial.println("Connected to WiFi!");
            connected = true;
        }
        delay(100);
    }
}

boolean on = false;
int pixel = 0;
int colour = 0;

WiFiClient client;

bool errored = false;

void loop() {
    if (client.connect(QUERY_HOST, QUERY_PORT)) {
        char query[256];
        sprintf(query, "GET %s HTTP/1.1\r\nHost: %s\r\nConnection: close\r\n\r\n", QUERY_PATH, QUERY_HOST);
        client.print(query);
    } else {
        Serial.println("Failed to connect to host");
    }

    String response;
    boolean dataComplete = false;
    int start_time = millis();

    char endOfHeaders[] = "\r\n\r\n";
    if (!client.find(endOfHeaders)) {
        Serial.println(F("Invalid response"));
        setAllPixels(strip.Color(255,0,0));
        errored = true;
        delay(5000);
        return;
    }

    // while(!dataComplete) {
    //     if (client.available() > 0) {
    //         String line = client.readStringUntil('\r');
    //         Serial.printf("Got data: %s\n", line.c_str());
    //         response += line;
    //     } else if (!client.connected()) {
    //         dataComplete = true;
    //         client.stop();
    //     } else if (millis() > start_time + 30000) {
    //         dataComplete = true;
    //         client.stop();
    //         Serial.println("Query timed out after 30 seconds");
    //     }
    // }

    DynamicJsonBuffer jsonBuffer(4096);

    // Parse JSON object
    JsonObject& root = jsonBuffer.parseObject(client);
    if (!root.success()) {
        Serial.println(F("Parsing failed!"));
        setAllPixels(strip.Color(255,0,0));
        errored = true;
        delay(5000);
        return;
    }

    if (errored) {
        errored = false;
        setAllPixels(strip.Color(0,0,0));
    }

    size_t ticket_size = root["tickets"].size();
    Serial.printf("%d pods\n", ticket_size);
    int light = 0;

    for (int jindex = 0; jindex < ticket_size; jindex++) {
        Serial.print(root["tickets"][jindex]["id"].asString());
        Serial.print(root["tickets"][jindex]["status"].asString());

        uint32_t colour;
        
        if (strcmp(root["tickets"][jindex]["status"].asString(), "green") == 0) {
            colour = strip.Color(0, 255, 0);
        } else if (strcmp(root["tickets"][jindex]["status"].asString(), "red") == 0) {
            colour = strip.Color(255, 0, 0);
        } else {
            colour = strip.Color(255, 191, 0);
        }

        for (int n=light; n < light + PIXEL_SIZE; ++n) {
            strip.setPixelColor(n, colour);
        }
        strip.show();

        light += PIXEL_SIZE + GAP_SIZE;
    }

    for (int n=light; n < PIXEL_COUNT; ++n) {
        strip.setPixelColor(n, strip.Color(0,0,0));
        strip.show();
    }

    delay(5000);
}